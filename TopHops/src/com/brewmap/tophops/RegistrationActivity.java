package com.brewmap.tophops;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class RegistrationActivity extends Activity {
	private EditText un;
	private EditText pw;
	
	private TextView error;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		
		un =(EditText)findViewById(R.id.reg_username);
		pw =(EditText)findViewById(R.id.reg_password);
		
		error = (TextView) findViewById(R.id.error_reg);
		
		Button deja = (Button) findViewById(R.id.deja_fait);
		deja.setOnClickListener(dejaListener);
	}
	View.OnClickListener dejaListener = new View.OnClickListener() {
        public void onClick(View v) {
        	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        	startActivity(i);
        }
      };
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}
	public void register(final View v){
		v.setEnabled(false);
		ParseUser user = new ParseUser();
		user.setUsername(un.getText().toString());
		user.setPassword(pw.getText().toString());
		user.signUpInBackground(new SignUpCallback() {
			@Override
			public void done(ParseException e) {
				if (e == null) {
					Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
					startActivity(i);
					finish();
		    } else {
		    	switch(e.getCode()){
				case ParseException.USERNAME_TAKEN:
					
					break;
				case ParseException.USERNAME_MISSING:
					
					break;
				case ParseException.PASSWORD_MISSING:
					
					break;
				default:
					
				}
		      // Sign up didn't succeed. Look at the ParseException
		      // to figure out what went wrong
		    }
			v.setEnabled(true);
		  }
		});
		

	}
}