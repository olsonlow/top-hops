package com.brewmap.tophops;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import android.content.Intent;
import com.parse.ParseUser;

public class FindBeersActivity extends Activity {

	private EditText fbr;
	private TextView bn;
	private TextView bd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_beers);
		
		fbr = (EditText) findViewById(R.id.beer_entry);
		bn = (TextView) findViewById(R.id.beer_name);
		bd = (TextView) findViewById(R.id.beer_desc);
	}
	
	public void findBeers(final View v){
		v.setEnabled(false);
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Beers");
		query.whereEqualTo("Name", fbr.getText().toString());
		
		query.findInBackground(new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
					bn.setText(objects.get(0).getString("Name").toString());
					bd.setText(objects.get(0).getString("BeerDescription").toString());
				} else {
				}
				v.setEnabled(true);
			}
		});
	}


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find_beers, menu);
		return true;
	}

}
