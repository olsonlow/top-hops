package com.brewmap.tophops;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;

public class MapView extends FragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.i("Maps", "MapView started with..." +getIntent().getExtras().getDouble("x") + getIntent().getExtras().getDouble("y") );
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mapview);
		
		LatLng BREWERY = new LatLng(getIntent().getExtras().getDouble("x"),getIntent().getExtras().getDouble("y"));
		FragmentManager fmanager = getSupportFragmentManager();
        Fragment fragment = fmanager.findFragmentById(R.id.map);
        SupportMapFragment supportmapfragment = (SupportMapFragment)fragment;
        GoogleMap supportMap = supportmapfragment.getMap();
        Marker brewery = supportMap.addMarker(new MarkerOptions().position(BREWERY).title(getIntent().getExtras().getString("position")));
        // Move the camera instantly to hamburg with a zoom of 15.
        supportMap.moveCamera(CameraUpdateFactory.newLatLngZoom(BREWERY, 15));

        // Zoom in, animating the camera.
        supportMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mapview, menu);
		return true;
	}

}