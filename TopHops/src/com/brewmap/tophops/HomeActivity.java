package com.brewmap.tophops;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class HomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		Button login = (Button) findViewById(R.id.loginBtn);
		Button signUp = (Button) findViewById(R.id.registerBtn);
		login.setOnClickListener(loginListener);
		signUp.setOnClickListener(registerListener);
		}
	View.OnClickListener loginListener = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        	startActivity(i);
        	}
		};
	View.OnClickListener registerListener = new View.OnClickListener() {
		public void onClick(View v) {
    		  Intent i = new Intent(getApplicationContext(), RegistrationActivity.class);
    		  startActivity(i);
    		  }
    	  };

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

}
