package com.brewmap.tophops;

import android.location.*;

import java.util.ArrayList;
import java.util.List;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.FindCallback;

import android.util.Log;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.Toast;
import android.os.Binder;

public class BreweryCommentList extends Activity implements OnItemClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_brewerycommentlist);	
		ListView lv = (ListView)findViewById(R.id.brewery_comment_list);
		lv.setClickable(true);
		ParseObject.registerSubclass(BreweryComments.class);
		ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>(this, new ParseQueryAdapter.QueryFactory<ParseObject>() {
		    public ParseQuery<ParseObject> create() {
		        // Here we can configure a ParseQuery to our heart's desire
				String brewery = getIntent().getExtras().getString("position");
		    	Toast.makeText(getApplicationContext(), "BreweryCommentList received "+ brewery + " from Locations" , Toast.LENGTH_LONG).show();
		        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("BreweryComments");
		        query.whereEqualTo("Brewery", brewery);
		        return query;
		      }
		});
		adapter.setObjectsPerPage(10);
		adapter.setTextKey("Comment");
		lv.setOnItemClickListener(this);
		lv.setAdapter(adapter);	
	}

	  public void onItemClick(AdapterView<?> l, View v, int position, long id) {
	        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
	/*      Object o = l.getAdapter().getItem(position);
	        Beers b = (Beers)o;
	        // Then you start a new Activity via Intent
	        Intent intent = new Intent();
	        intent.setClass(this, BeerProfile.class);
	        intent.putExtra("position", b.getName());
	        // Or / And
	        intent.putExtra("id", b.getObjectId());

	        startActivity(intent);
	        */
	    }	

		  	@Override
		  	public boolean onCreateOptionsMenu(Menu menu) {
		  		// Inflate the menu; this adds items to the action bar if it is present.
		  		getMenuInflater().inflate(R.menu.brewerycommentlist, menu);
		  		return true;
		  	}

		  }
