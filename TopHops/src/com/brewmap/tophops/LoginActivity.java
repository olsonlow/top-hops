package com.brewmap.tophops;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class LoginActivity extends Activity {
	private EditText unl;
	private EditText pwl;
	private TextView error;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		unl = (EditText) findViewById(R.id.log_username);
		pwl = (EditText) findViewById(R.id.log_password);
		error = (TextView) findViewById(R.id.login_error);
		Button newuser = (Button) findViewById(R.id.newUser);
		newuser.setOnClickListener(registerListener);
	}
  	View.OnClickListener registerListener = new View.OnClickListener() {
        public void onClick(View v) {
        	Intent i = new Intent(getApplicationContext(), RegistrationActivity.class);
        	startActivity(i);
        }
      };

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	public void loginEditTexts(final View v){
		v.setEnabled(false);
		
		ParseUser.logInInBackground(unl.getText().toString(), pwl.getText().toString(), new LogInCallback(){
			@Override
			public void done(ParseUser user, ParseException e){
				if (user!= null){
					Intent i = new Intent(LoginActivity.this, TopHopsActivity.class);
					startActivity(i);
					finish();
				}else{
					//sign up failed
				}
				v.setEnabled(true);
			}
		});
	}
}
