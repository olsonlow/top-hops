package com.brewmap.tophops;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class TopHopsActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_top_hops);
		
		Button findBeers = (Button) findViewById(R.id.FindBeers);
		Button findBrewery = (Button) findViewById(R.id.FindBrewery);
		Button beerDict = (Button) findViewById(R.id.BeerDictionary);
		Button breweryLocation = (Button) findViewById(R.id.BreweryLocation);
		
		findBeers.setOnClickListener(beerListener);
		findBrewery.setOnClickListener(breweryListener);
		beerDict.setOnClickListener(dictListener);
		breweryLocation.setOnClickListener(locationListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.top_hops, menu);
		return true;
		}
	View.OnClickListener beerListener = new View.OnClickListener() {
		public void onClick(View v) {
			Intent i = new Intent(getApplicationContext(), FindBeersActivity.class);
			startActivity(i);
			}
		};

	View.OnClickListener breweryListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), FindBreweryActivity.class);
            startActivity(i);
            }
          };

    View.OnClickListener dictListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(getApplicationContext(), GlossaryActivity.class);
            startActivity(i);
            }
         };
   View.OnClickListener locationListener = new View.OnClickListener() {
    	public void onClick(View v) {
    		Intent i = new Intent(getApplicationContext(), Locations.class);
    		startActivity(i);
    		}
    	};
}
