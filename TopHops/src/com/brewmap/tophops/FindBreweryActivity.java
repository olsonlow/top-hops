package com.brewmap.tophops;

import java.util.List;
import com.parse.FindCallback;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.parse.ParseUser;

public class FindBreweryActivity extends Activity {

	private EditText fbr;
	private TextView bn;
	private TextView ba;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_brewery);
		
		fbr = (EditText) findViewById(R.id.brewery_entry);
		bn = (TextView) findViewById(R.id.brewery_name);
		ba = (TextView) findViewById(R.id.brewery_address);
	}
		
	public void findBrewery(final View v){
		v.setEnabled(false);
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Brewery");
		query.whereEqualTo("Name", fbr.getText().toString());
		
		query.findInBackground(new FindCallback<ParseObject>(){
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e == null) {
				
					bn.setText(objects.get(0).getString("Name").toString());
					ba.setText(objects.get(0).getString("Address").toString());
				} else {
				}
				v.setEnabled(true);
			}
		});
	}


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find_brewery, menu);
		return true;
	}

}