package com.brewmap.tophops;
import java.io.File;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;

@ParseClassName("Brewery")
public class Brewery extends ParseObject{
	public Brewery(){
		
	}

	public String getAddress() {
		// TODO Auto-generated method stub
		return getString("Address");
	}

	public String getName() {
		// TODO Auto-generated method stub
		return getString("Name");
	}
	
	public ParseFile getLogo(){
		return getParseFile("Logo");
	}
}
