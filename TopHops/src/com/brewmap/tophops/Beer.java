package com.brewmap.tophops;
import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Beer")
public class Beer extends ParseObject{
	public Beer(){
		
	}

	public String getAddress() {
		// TODO Auto-generated method stub
		return getString("Address");
	}

	public String getName() {
		// TODO Auto-generated method stub
		return getString("Name");
	}
}
