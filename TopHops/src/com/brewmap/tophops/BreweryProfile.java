package com.brewmap.tophops;

import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BreweryProfile extends Activity{
	private TextView breweryName, mapButton, beersButton;
	private Button commentsButton;
	private EditText makeComment;
	String brewery;
	@Override
	protected void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_brewery_profile);
			Toast.makeText(getApplicationContext(), "BreweryProfile received "+ brewery + " from Locations", Toast.LENGTH_LONG).show();
			
			breweryName = (TextView)findViewById(R.id.breweryN);
			brewery = getIntent().getExtras().getString("position");
			breweryName.setText(brewery);
			
			mapButton = (TextView)findViewById(R.id.viewMap);
			beersButton = (TextView)findViewById(R.id.viewBeers);
			commentsButton = (Button)findViewById(R.id.submit);
			
			mapButton.setOnClickListener(mapListener);
			beersButton.setOnClickListener(beerListener);
			commentsButton.setOnClickListener(commentListener);
			
			ParseObject.registerSubclass(BreweryComments.class);
			
	}
	
	View.OnClickListener mapListener = new View.OnClickListener(){
		public void onClick(View v){
		String bName = getIntent().getExtras().getString("position");
		
		Bundle u = new Bundle();
        u.putDouble("x", getIntent().getExtras().getDouble("x"));
        u.putDouble("y", getIntent().getExtras().getDouble("y"));
		
        Intent i = new Intent(getApplicationContext(), MapView.class);
     	i.putExtra("position", bName);
        i.putExtras(u);
		startActivity(i);
		}
	};
	View.OnClickListener beerListener = new View.OnClickListener(){
		public void onClick(View v){
		String bName = getIntent().getExtras().getString("position");
		Bundle u = new Bundle();
        u.putDouble("x", getIntent().getExtras().getDouble("x"));
        u.putDouble("y", getIntent().getExtras().getDouble("y"));

        Intent i = new Intent(getApplicationContext(), BeerList.class);	
        i.putExtra("position", bName);
        i.putExtras(u);
		startActivity(i);
		}
	};
	View.OnClickListener commentListener = new View.OnClickListener(){
		public void onClick(View v){
		
		final String bName = getIntent().getExtras().getString("position");
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Brewery");
		query.whereEqualTo("Name", bName);
		
		String objectId = getIntent().getExtras().getString("objectId");
		makeComment = (EditText)findViewById(R.id.commentEntry);
		
		query.getInBackground(objectId, new GetCallback<ParseObject>(){
			public void done(ParseObject object, ParseException e) {
				if (e == null) {

					ParseUser user = ParseUser.getCurrentUser();
					BreweryComments bc = new BreweryComments();
					
					bc.put("User", user.getUsername());
					bc.put("Creator", user);
					bc.put("Brewery", brewery);
					bc.put("BreweryPtr", object);
					bc.put("Comment", makeComment.getText().toString());
					try {
						bc.save();
					} catch (ParseException e1) {
						e1.printStackTrace();
					}
					Bundle u = new Bundle();
			        u.putDouble("x", getIntent().getExtras().getDouble("x"));
			        u.putDouble("y", getIntent().getExtras().getDouble("y"));
			        Intent i = new Intent(getApplicationContext(), BreweryCommentList.class);
			        i.putExtra("position", bName);
			        i.putExtras(u);
					startActivity(i);
				} else {
				}
			}
		});
		
		
		}
	};
}
