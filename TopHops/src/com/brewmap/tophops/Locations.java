package com.brewmap.tophops;
import android.location.*;

import java.util.ArrayList;
import java.util.List;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.FindCallback;

import android.util.Log;
import android.view.Menu;
import android.widget.AdapterView.*;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.Toast;
import android.os.Binder;
public class Locations extends Activity implements OnItemClickListener {
	private LocationService locService;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_locations);
		startService(new Intent(LocationService.class.getName()));
		ParseUser currentUser = ParseUser.getCurrentUser();
		locService = new LocationService(Locations.this);
		if (locService.canGetLocation()){
			double latitude = locService.getLatitude();
			double longitude = locService.getLongitude();
			ParseGeoPoint point = new ParseGeoPoint(latitude, longitude);
			currentUser.put("geoloc", point);
			currentUser.saveInBackground();
			Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();    
        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            locService.showSettingsAlert();
		}
		
		ListView lv = (ListView)findViewById(R.id.task_list);
		lv.setClickable(true);
		ParseObject.registerSubclass(Brewery.class);
		ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>(this, new ParseQueryAdapter.QueryFactory<ParseObject>() {
	    public ParseQuery<ParseObject> create() {
	        // Here we can configure a ParseQuery to our heart's desire.
	        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Brewery");
	        ParseUser currentUser = ParseUser.getCurrentUser();
			ParseGeoPoint userLocation = (ParseGeoPoint) currentUser.get("geoloc");
			//ParseGeoPoint userLocation = new ParseGeoPoint(38.33864, -122.6741); //For testing
			query.whereNear("brewloc", userLocation);
			query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK);
	        return query;
	      }
		});
		adapter.setObjectsPerPage(10);
		adapter.setTextKey("Name");
		//adapter.setImageKey("LogoSm");
		lv.setOnItemClickListener(this);
		lv.setAdapter(adapter);

	}		
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
    	Object o = l.getAdapter().getItem(position);
    	Brewery b = (Brewery)o;
    	Toast.makeText(getApplicationContext(), "You clicked Item: " + id +"and brewery" + ((Brewery) o).getName()+ " at position:" + position, Toast.LENGTH_LONG).show();    
        // Then you start a new Activity via Intent	
    	Intent intent = new Intent();
        Bundle u = new Bundle();
        u.putDouble("x", b.getParseGeoPoint("brewloc").getLatitude());
        u.putDouble("y", b.getParseGeoPoint("brewloc").getLongitude());
        
        intent.setClass(this, BreweryProfile.class);
        intent.putExtras(u);
        intent.putExtra("position", b.getName());
        intent.putExtra("objectId", b.getObjectId());
        // Or / And
        intent.putExtra("id", id);
        startActivity(intent);
    }	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.locations, menu);
		return true;
	}



}




